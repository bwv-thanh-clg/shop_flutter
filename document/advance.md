# Dart Advance

## 例外処理（try-catch）

```dart
main() {
  void errorFunc() {
    try {
      // throw Exceptionで意図的に例外を投げる
      throw Exception('例外です');
    } on Exception catch(e) {
      // 捕まえる型を指定するには on ~~ catch を使う
      // eはException型
      print(e);
      // rethrowでtry-catch-finallyブロックの外に例外を投げ直す事ができる（関数の外などでcatchする必要あり）
      rethrow;
    } finally {
      // finallyブロックは例外の有無にかかわらず実行される、省略可。
      print('finally');
    }
  }

  // 例外処理：try-catch文
  try {
    errorFunc();
  } catch (e, s) {
    // 型を指定しないcatchは、何型かわからない例外全部キャッチする
    // catchに仮引数を２つ指定すると、２つ目はStackTraceオブジェクトが入る
    print(s);
  }
}
```

## クラス

また、C++のクラスライクに初期化子で変数を初期化することもできます。（メンバ変数がfinal、constの場合に使う）<br/>
メンバ関数上書きする際（オーバライド）、対象のメンバ関数にメタデータの@overrideをつけます（ただし、任意です）<br/>
private定義をするには_をつけます。ただし、同ライブラリ内だとアクセスできてしまいます。<br/>
静的メンバを定義する（クラス共通のメンバ変数、メンバ関数）にはstaticキーワードをつけます。（他の言語と同じ）<br/>

```dart
main() {
  // インスタンス生成
  var person1 = new Person('Yamada', 'Taro');
  print('${person1.firstName} ${person1.lastName}');
  // メンバ関数呼び出し
  person1.greed();
  // 名前付きコンストラクタ呼び出し
  var person2 = new Person.origin();
  print('${person2.firstName} ${person2.lastName}');

  // 静的メンバはインスタンス化しなくても呼び出し可能（クラス共通の関数、変数）
  print(Person.capacity);
  Person.staticMethod();

  // callメソッドの呼び出し
  print(person1());

  // 実は同ライブラリ内であれば、privateにアクセスできてしまう
  person2._member = 'abc';
  print(person2._member);

  var engineer1 = new Engineer('エン', 'ジニア');
  engineer1.greed();
  // 親クラスにキャストできる
  Person engineer2 = Engineer.instance(true);
  // 呼ばれるのは継承クラスのgreed(ポリモーフィズム)
  engineer2.greed();
  // 明示的に元のクラスにキャストするにはasを使う
  (engineer2 as Engineer).greed();
}

class Person {
  String firstName;
  String lastName;

  // this.フィールド名の引数だけで、フィールドに値を代入できる
  // コンストラクタのブロック内ではすでに代入された状態で使用できる
  // thisを省略すると別の仮引数として扱われてしまう
  Person(this.firstName, this.lastName);

  // privateメンバは便宜上、_から始まる。ただし、同ライブラリ内であればアクセスしようと思えばアクセスできてしまう。
  String _member;

  // 名前付きコンストラクタ
  // 複数のコンストラクタをもたせたいときに使う
  Person.origin() {
    this.firstName = '氏';
    this.lastName = '名';
    // 実はメソッド内はthisを省略してもクラス内のフィールドにアクセスできる
    // firstName = '氏';
    // lastName = '名';
  }

  // メンバ関数（インスタンス化したら呼び出し可能）
  greed() {
    print('Hello ${firstName} ${lastName}');
  }

  // 同名メソッドは作成できない（メソッドのオーバーロード）
  // 引数で処理を分けたい同名メソッドを作りたければ任意引数を使う
  // greed(int a) {}

  // 静的メンバ変数
  static const capacity = 16;

  // 静的メンバ関数
  static void staticMethod() {
    print('Hello');
  }

  // callは特殊なメンバ関数
  // インスタンス名()で呼び出しできる(呼び出し可能なクラス)
  call() => '$firstName $lastName';
}

// extendsでクラスの継承（親クラスのメンバが参照できる）
class Engineer extends Person {
  final String name;

  // 親クラスのコンストラクタを呼ぶには初期化子（:）でsuperを使う
  // メンバ変数の初期化も初期化子内で行える
  Engineer(String firstName, String lastName) : 
    name = '', 
    super(firstName, lastName);

  Engineer.origin():
    name = 'hello',
    super.origin() {
    print('${firstName} ${lastName}');
  }

  // 先頭にfactoryキーワードをつけるとファクトリーコンストラクタとなる
  // 自身のインスタンスを戻り値として返すことを明示できる
  factory Engineer.instance(bool isEngineer) {
    var instance = isEngineer ? new Engineer.origin() : new Person.origin();
    return instance;
  }

  // メソッドの上書き
  // @overrideは任意（けど書いたほうがわかりやすい）
  @override
  greed() {
    // super.greed(); // メンバ関数内でコンストラクタ以外の親クラスのメンバ関数はsuper.メンバ関数名で呼び出しできる
    print('I am ${firstName}${lastName}');
  }
}
```

不変コンストラクタはメンバ変数が全てfinalの場合に定義できます。
この場合、インスタンス化された変数はコンパイル定数として扱われます。
一部処理を別コンストラクタに移譲するリダイレクトコンストラクタも作成できます。

```dart
main() {
  const p = Point(1, 2);
  print('x=${p.x},y=${p.y}');
  var pp = Point.alongXAxis(3);
  print('x=${pp.x},y=${pp.y}');
}

class Point {
  final int x;
  final int y;

  // 不変コンストラクタ
  // メンバ変数が全部 final 宣言されていると、コンストラクタの頭にconstをつけられる
  // この場合、インスタンス化された変数はコンパイル定数として扱われる
  const Point(this.x, this.y);

  // リダイレクトコンストラクタ
  // 別のコンストラクタに一部処理を移譲する
  const Point.alongXAxis(int x): this(x, 0);
}
```

## ゲッター、セッター

```dart
main (){
  var rect = Rectangle(3, 4, 20, 15);
  print('right=${rect.right}'); // rightを参照するとゲッターが呼ばれて計算結果を取得。left + width
  rect.right = 12; // rightを変えるとセッターがよばれて
  print('left=${rect.left}'); // leftの結果も変わる
}

class Rectangle {
  int left, top, width, height;

  Rectangle(this.left, this.top, this.width, this.height);

  // ゲッター：rightのパラメータを参照できる。（実態はleft+widthの計算結果を返す）
  int get right => left + width;
  // セッター：パラメータを代入時にleftを計算する
  set right(num value) => left = value - width;
  int get bottom => top + height;
  set bottom(num value) => top = value - height;
}
```

## 抽象クラス

```dart
main() {
  // abstractクラスはインスタンス化できない
  // var animal = new Animal();
  var cat = new Cat();
  cat.hello();
}

// 抽象クラス
// 処理未定義のメンバ関数を持つクラス、インスタンス化できない
abstract class Animal {
  void hello();
}

// 抽象クラスを継承して未定義メンバ関数を実装する
class Cat extends Animal {
  void hello() {
    print("みゃお");
  }
}
```

## インタフェース

```dart
main() {
  // Masterクラスのインスタンスを作成
  var master = new Master('Master');
  print(master.commit('ToDo List'));
  // Masterインタフェースを継承したBranchクラスのインスタンスを作成
  var branch = new Branch();
  print(branch.commit('sort'));

  var director = Director('Tanaka', 'Saburo');
  director.hello();
  director.story();
}

// Dartには interfaceキーワードが存在しませんが、クラスを宣言した時点でそのクラスと同じAPIのinterfaceが勝手に作られます（暗黙的なinterface）
// インターフェイスは実装を持たない
// Masterクラスの宣言であり、commit()メソッドを持ったMasterインターフェイスの宣言でもある
class Master {
  // privateなものはインターフェイスには含まれない
  final _name;

  // コンストラクタもインターフェイスには含まれない
  Master(this._name);

  String commit(String msg) => '${_name} commit ${msg}';
  // このメンバ関数の宣言のみインターフェイスに含まれる（実装は含まれない）
  // String commit(String msg);
}

// implementsでPersonインターフェイスを実装する
class Branch implements Master {
  // privateメンバ変数に関してはゲッターの実装をしないと怒られる
  get _name => '';

  // commitを実装しないと怒られる
  String commit(String msg) => 'Branch commit ${msg}';
}

// extendsの親クラスは１つしか指定できないのに対し、implementsは複数指定できる（Javaと一緒）
// 抽象クラスもimplementsできる
class Director extends Person implements Animal, Point {
  // Personのコンストラクタを継承
  Director(String firstName, String lastName) : super(firstName, lastName);

  // Animalのメンバ関数
  @override
  void hello() {
    print('I am Director');
  }

  // Pointのゲッター実装
  @override
  int get x => x;

  // Pointのゲッター実装
  @override
  int get y => y;

  // Director独自のメンバ関数
  void story() {
    print('Yes we can');
  }
}
```

## ミックスイン

```dart
main() {
  // mixin
  var musician = new Musician();
  // Performerクラスの実装を呼び出す
  musician.PerformerMethod();
  // Musicalクラスの実装を呼び出す
  musician.MusicalMethod();
}

class Performer {
  Performer() {
    print('Perfomer');
  }
  void PerformerMethod() {
    print('PerformerMethod');
  }
}
mixin Musical {
  // mixinはコンストラクタは定義できない
  void MusicalMethod() {
    print('MusicalMethod');
  }
}

// ミックスイン、with句でつないだmixinの実装が使える
// 多重継承に似ているが、コンストラクタが定義できるのはextendsしたクラスのみ
class Musician extends Performer with Musical {
  Musician(): super() {
    print('Musician');
  }
}
```

## ジェネリックス

```dart
main() {
  // ジェネリックスのインスタンス化は型を指定する
  var cache = new Cache<String>();
  cache.setByKey('key', 'test');
  print('key=${cache.getByKey('key')}');

  // メソッドのジェネリックス
  // 型の制限をするときは型のextendsを使う
  T sum<T extends num>(List<T> list, T init){
    T sum = init;
    list.forEach((value) { 
      sum += value;
    });
    return sum;
  }

  int r1 = sum<int>([1,2,3], 0);
  print(r1);
  // 型指定しない場合は左辺の型に暗黙的に型推定される
  double r2 = sum([1.1, 2.2, 3.3], 0.0);
  print(r2);
}

// 型だけが違う実装をしたクラスを実装したい場合はジェネリックスを使うと便利
// インスタンス化するときTに型を指定する
class Cache<T> {
  Map<String, T> store = <String, T>{};

  T getByKey(String key) {
    return store[key];
  }

  void setByKey(String key, T value) {
    this.store.addAll(<String, T>{key: value});
  }
}
```

### typedef

関数に別名（型）を定義することが出来ます。<br>
Dartの場合、typedefにジェネリックスを併用することが出来ます。<br>

```dart
main() {
  // sort関数をtypedefで定義した型で格納
  Compare<int> sortFunc = sort;
  print('sort: ${sortFunc(1, 2)}');
}

// typedefで関数の別名（型）を定義できる
typedef Compare<T> = T Function(T a, T b);
int sort(int a, int b) => a - b;
```

## 非同期関数 (async/await)

非同期関数を作成するにはFuture型を返却します。<br>
Futureに関してはJavaScriptのPromiseと似ています。<br>
async/awaitが使えるため、awaitで同期待ちすることができます。

```dart
import 'dart:async'; // 非同期処理

main() async {
  // 非同期関数（async）、Future<データ型>で戻り値を返却する必要がある
  // JavaScriptのPromise関数とほぼ同じ
  Future<String> lookUpVersion() async => '1.0.0';
  var version = await lookUpVersion(); // awaitで同期待ち（async関数内でのみ使える）
  print(version);
}
```

## ジェネレータ

...