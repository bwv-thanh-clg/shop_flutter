# Dart Basic

## main関数

dartの処理の実行はmain関数から始まります。

## 基本的なデータ型

- 数値：int, double
- 文字列：String
- 真偽：bool
- 配列：List
- セット（ユニークな配列）：Set
- Key/Valueペア：Map

```dart
int b = 10;
double c = 12.3;
String d = 'abc';
bool e = true;
List f = [1,2,3]; // 配列
f.add(4);  // 末尾追加

Set g = {'a', 'b', 'c'}; // 重複を許さない配列
g.add('d');  // 末尾追加
g.add('d');  // 重複して入ることはない

// Key/Valueペア
Map h = {
  // Key: Value
  'first': 'one',
  'second': 'two',
  'third': 'three'
};
h.addAll({'fourth': 'four'});
```

- 別のデータ型への変換

```dart
print(int.parse('42') == 42); // String => int
print(double.parse('42.3') == 42.3); // String => double
print(42.31.toString() == '42.31'); // number => String
```

## 定数

`final、const`を使うことで定数を定義できます。<br/>
値が変更されないことが保証される

```dart
main() {
  final k = 1; // finalの変数は初期化時のみ代入可能
  // k = 2; // 再代入しようとするとエラー
  const l = 1; // 定数、再代入はエラーになる(コンパイル時に埋め込まれる)
  // l = 2;
  List m = const [1, 2, 3]; // 定数配列を代入
  // m.add(1); // 定数配列の中身の変更はできない（実行時エラー：Cannot add to an unmodifiable list）
  print('$k, $l, $m');
}
```
finalは変数の性質で、constは変数の性質＋値の性質も規定しています（constの方が制限出来る範囲が広義）

## 制御文

if, forEach, for, for in, while, do while, switchなどの基本的な制御文が使えます。<br/>
switch文のみ他の言語と少し違いが有り、連続で実行するためにラベルを明示的に付ける必要があります。

```dart
main() {
  // forEach
  var lists = ['l', 'i', 's', 't'];
  lists.forEach((value) { print(value); });
  var sets = {'s', 'e', 't'};
  sets.forEach((value) { print(value); });
  var maps = {'k': 'm', 'e': 'a', 'y': 'p'};
  maps.forEach((key, value) { print('$key $value'); });

  // for
  for (var i = 0; i < 3; i++) {
    print(i);
  }

  // for in
  for (var i in ['f', 'o', 'r', 'i', 'n']) {
    if (i == 'o' || i == 'r') continue;
    print(i);
  }
  for (var i in {'s', 'e', 't'}) {
    print(i);
  }

  // while
  var w = 0;
  while (true) {
    // if文
    if (w == 3) {
      break;
    } else if(w == 1) {
      w++;
      print('wは$w');
    } else {
      w++;
      print('w=$w');
    }
  }

  // do-while
  var dw = 0;
  do {
    dw++;
    print('dw=$dw');
  } while (dw < 3);

  // switch
  var command = 'CLOSED';
  switch (command) {
    case 'CLOSED':
      print('CLOSED');
      continue nowClosed;  // continueの場合、nowClosedラベルを実行する
    nowClosed:
    case 'NOW_CLOSED':
      print('NOW_CLOSED');
      break;
  }
}
```

## 関数

```dart

// 関数（処理のまとまりを記述する）
// 戻り値の型を指定しない場合はdynamicになる（あまり推奨されていない）
// 戻り値がいらない場合は戻り値にvoidを指定する
int testFunction(){
  const y = 20;
  print('$x, $y'); // 関数の外で定義されているxは参照できる
  return x + y;
}

main() {
  const x = 10;
  // print('$y'); // 関数の内部で定義されているyは参照できない（変数のスコープ）

  // トップレベルでなくても関数内部でも関数定義できる
  // int testFunction(){
  //   const y = 20;
  //   print('$x, $y'); // 関数の外で定義されているxは参照できる
  //   return x + y;
  // }
  var result = testFunction();
  print('$result');

  // 一行の場合、ファットアローで省略記法がかける（JavaScriptのアロー関数と同じ）
  int oneline(a,b) => a + b;
  // この関数と等価
  // int oneline(a,b){ return a + b }
  print(oneline(1,2));

  // {}は名前付き任意引数
  void enableFlags({bool bold, bool hidden}) { print('$bold $hidden'); }
  // 引数ラベルをつけて呼び出す(記述順序は任意)
  // boldにはnull、hiddenにはtrueが渡される
  enableFlags(hidden: true);

  // []は順序付き任意引数、特定の位置以降の引数を省略可能
  // 任意引数にはデフォルト値をもたせることも可能（nullの場合、=の右辺の値が代入される）
  String say(String from, String msg, [String device = 'unknown', String mood]) {
    // ?? 演算子はnullの場合に右側の値が適応される
    return '$from says $msg platform: ${device} mood: ${mood ?? 'unknown'}';
  }
}
```

## 列挙型 (Enum)

列挙型はデータの識別用に定義しておくと便利です。<br>
indexで0から始まるインデックスにアクセスできます。<br>

```dart
main() {
  var c = Color.blue;
  print(Color.green.index == 1);
  // 列挙型はswitchの条件分岐に使える
  switch (c) {
    case Color.green:
      print('green');
      break;
    case Color.blue:
      print('blue');
      break;
    case Color.red:
      print('red');
      break;
    default:
  }
}

// 列挙型
// 列挙子は宣言された順にインデックス（0始まり）が割り振られていて、 index で参照できる。
// enumを継承できなかったり（mixinにも使えない）、enumのインスタンスを自前で生成できない（定数のみしか使えない）
// 実装を持つことが出来ない以外、Javaとほぼ同じ
enum Color { red, green, blue }
```