# DART

#### Tutorial

https://qiita.com/teradonburi/items/913fb8c311b9f2bdb1dd

#### Cheatsheet

https://dart.dev/codelabs/dart-cheatsheet

#### Gallery

https://gallery.flutter.dev/#/

#### Sample app

https://flutter.github.io/samples/#

## Core library

### dart:core
Built-in types, collections, and other core functionality. This library is **automatically imported into every Dart program**.

#### dart:async
Support for asynchronous programming, with classes such as Future and Stream.

#### dart:math
Mathematical constants and functions, plus a random number generator.

#### dart:convert
Encoders and decoders for converting between different data representations, including JSON and UTF-8.

#### dart:html
DOM and other APIs for browser-based apps.

#### dart:io
I/O for programs that can use the Dart VM, including Flutter apps, servers, and command-line scripts.

### Library

Another library can find in https://pub.dev/ and [Dart web developer library guide](https://dart.dev/web/libraries)

## Basic

[Readmore](./basic.md)

## Advance (class, try catch, object)

[Readmore](./advance.md)